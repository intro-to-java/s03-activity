package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");
        Scanner in = new Scanner(System.in);
        try {
            int num = in.nextInt();
            if(num == 0 || num < 0) {
                System.out.println("Invalid input");
            } else {
                int answer = 1;
                int counter = 1;

                while(counter <= num) {
                    answer = answer * counter;
                    counter++;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            }

        } catch(Exception e) {
            System.out.println("Invalid input");
            e.printStackTrace();
        }




    }
}
